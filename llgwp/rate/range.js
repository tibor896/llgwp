jQuery(document).on('gform_post_render', function(){
    
    if (jQuery("#input_4_6").val() != "undefined" && jQuery("#input_4_16").val() != "undefined") {
        
        jQuery(".customer-name span").html(jQuery("#input_4_6").val() + " " + jQuery("#input_4_16").val());
        jQuery(".customer-name").css("display","block");
        
    }
    
});

jQuery(document).ready(function(){
    if (jQuery( ".borrow-range" ).length) {
    
        jQuery(".borrow-range").asRange({
            step: 1000,
            limit: true,
            range: false,
            min: 5000,
            max: 250000,
            format: function(value) {
                jQuery("#input_8_71").val(value);
                return '$' + value.toLocaleString("en-US");
            }
        });
    
    }
});
