<?php if(get_the_ID() != 3612 && get_the_ID() != 3801): // Home Page?>

    <form class="landing-form code-form" action="/apply-online/" method="post">

        <p><i class="fas fa-envelope"></i> <strong> Responding to a mail offer?</strong></p>
        <p><b>Step 1:</b> Enter your eligibility ID or offer code:</p>

<!--        <input type="text" name="code" placeholder="XXXX-XXX-XXX" class="inputmask" data-inputmask="'mask': '****-***-***'" />-->
        <input type="text" name="code" />

        <p><a href="<?php echo site_url(); ?>/apply-online/?is_user_new=1">I don't have an offer code</a></p>
        
        <button type="submit" class="button">GET MY RATE</button>

        <p><strong><u>Checking your rate DOES NOT impact your credit score.</u></strong></p>

    </form>
    
<?php else: // Get Your Rate Page?>    

    <?php
    $form_url = get_the_permalink(3612);
    if(get_the_ID() == 3801){
       $form_url = get_the_permalink(3801); 
    }
    ?>

    <div class="rate-code">

    <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_4"><a id="gf_4" class="gform_anchor"></a><form method="post" enctype="multipart/form-data" id="gform_4" action="<?php echo $form_url; ?>">
        
        <div class="progress-custom">
    
            <p>Step 1 Of 3</p>
            
            <div class="progress-status" style="width: 33%;">
                <p>Step 1 Of 3</p>
            </div>
        
        </div>
            
                        <div class="gform_body"><div id="gform_page_4_1" class="gform_page" style="display: block;">
                                    <div class="gform_page_fields"><ul id="gform_fields_4" class="gform_fields top_label form_sublabel_below description_below"><li id="field_4_12" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_4_12">Enter your eligibility ID<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_text">
                                    
<!--                                    <input name="code" placeholder="XXXX-XXX-XXX" class="medium inputmask" data-inputmask="'mask': '****-***-***'" id="input_4_12" type="text" value="" aria-required="true" aria-invalid="false">-->
                                    <input name="code" id="input_4_12" type="text" value="" aria-required="true" aria-invalid="false">

                                    </div></li><li id="field_4_13" class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible"><p>* Checking your loan options does not affect your credit score.<br><br><a href="">Not sure if you have an eligibility ID? Click here.</a></p></li></ul>
                    </div>
                    <div class="gform_page_footer">
                         <input type="submit" id="gform_next_button_4_10" class="gform_next_button button" value="Next"/> 
                    </div>
                </div>
            </div>
        </form>
    </div>
    
    </div>


<?php endif; ?>