		<?php
			
		do_action( 'ava_before_footer' );	
			
		global $avia_config;
		$blank = isset($avia_config['template']) ? $avia_config['template'] : "";

		//reset wordpress query in case we modified it
		wp_reset_query();


		//get footer display settings
		$the_id 				= avia_get_the_id(); //use avia get the id instead of default get id. prevents notice on 404 pages
		$footer 				= get_post_meta($the_id, 'footer', true);
		$footer_widget_setting 	= !empty($footer) ? $footer : avia_get_option('display_widgets_socket');


		//check if we should display a footer
		if(!$blank && $footer_widget_setting != 'nofooterarea' )
		{
			if( $footer_widget_setting != 'nofooterwidgets' )
			{
				//get columns
				$columns = avia_get_option('footer_columns');
		?>
        
            <?php $custom_header_pages = array(3801,3612,3651,3665,3526); ?>
            
            <?php if (!in_array(get_the_ID(), $custom_header_pages)): ?>
        
				<div class='container_wrap footer_color' id='footer'>

					<div class='container'>

						<?php
						do_action('avia_before_footer_columns');

						//create the footer columns by iterating

						
				        switch($columns)
				        {
				        	case 1: $class = ''; break;
				        	case 2: $class = 'av_one_half'; break;
				        	case 3: $class = 'av_one_third'; break;
				        	case 4: $class = 'av_one_fourth'; break;
				        	case 5: $class = 'av_one_fifth'; break;
				        	case 6: $class = 'av_one_sixth'; break;
				        }
				        
				        $firstCol = "first el_before_{$class}";

						//display the footer widget that was defined at appearenace->widgets in the wordpress backend
						//if no widget is defined display a dummy widget, located at the bottom of includes/register-widget-area.php
						for ($i = 1; $i <= $columns; $i++)
						{
							$class2 = ""; // initialized to avoid php notices
							if($i != 1) $class2 = " el_after_{$class}  el_before_{$class}";
							echo "<div class='flex_column {$class} {$class2} {$firstCol}'>";
							if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer - column'.$i) ) : else : avia_dummy_widget($i); endif;
							echo "</div>";
							$firstCol = "";
						}

						do_action('avia_after_footer_columns');

						?>


					</div>


				<!-- ####### END FOOTER CONTAINER ####### -->
				</div>
                
            <?php endif; ?>

	<?php   } //endif nofooterwidgets ?>



			

			<?php

			//copyright
			$copyright = do_shortcode( avia_get_option('copyright', "&copy; ".__('Copyright','avia_framework')."  - <a href='".home_url('/')."'>".get_bloginfo('name')."</a>") );

			// you can filter and remove the backlink with an add_filter function
			// from your themes (or child themes) functions.php file if you dont want to edit this file
			// you can also remove the kriesi.at backlink by adding [nolink] to your custom copyright field in the admin area
			// you can also just keep that link. I really do appreciate it ;)
			$kriesi_at_backlink = kriesi_backlink(get_option(THEMENAMECLEAN."_initial_version"), 'Enfold');


			
			if($copyright && strpos($copyright, '[nolink]') !== false)
			{
				$kriesi_at_backlink = "";
				$copyright = str_replace("[nolink]","",$copyright);
			}

			if( $footer_widget_setting != 'nosocket' )
			{

			?>
            
                <?php if (!in_array(get_the_ID(), $custom_header_pages)): ?>

                    <footer class='container_wrap socket_color' id='socket' <?php avia_markup_helper(array('context' => 'footer')); ?>>
                    
                        <div class='container'>

                            <span class='copyright'>© <?php echo date("Y"); ?> Liberty Lending Group. All Rights Reserved.</span>

                            <?php
                                if(avia_get_option('footer_social', 'disabled') != "disabled")
                                {
                                    $social_args 	= array('outside'=>'ul', 'inside'=>'li', 'append' => '');
                                    echo avia_social_media_icons($social_args, false);
                                }
                            
                                
                                    $avia_theme_location = 'avia3';
                                    $avia_menu_class = $avia_theme_location . '-menu';

                                    $args = array(
                                        'theme_location'=>$avia_theme_location,
                                        'menu_id' =>$avia_menu_class,
                                        'container_class' =>$avia_menu_class,
                                        'fallback_cb' => '',
                                        'depth'=>1,
                                        'echo' => false,
                                        'walker' => new avia_responsive_mega_menu(array('megamenu'=>'disabled'))
                                    );

                                $menu = wp_nav_menu($args);
                                
                                if($menu){ 
                                echo "<nav class='sub_menu_socket' ".avia_markup_helper(array('context' => 'nav', 'echo' => false)).">";
                                echo $menu;
                                echo "</nav>";
                                }
                            ?>

                        </div>

                    <!-- ####### END SOCKET CONTAINER ####### -->
                    </footer>
                
                <?php endif; ?>
                
                <div class="footer-disclosure avia-section-large">
                
                    <div class="content">
                
                        <div class='container'>
                    
                            <p>The operator of this website, Liberty Lending Group, LLC intends to provide loan referral services through a third-party lender. Liberty Lending Group, LLC does not make personal loans or credit decisions. Providing your information on this website does not guarantee that you will be approved for a personal loan.</p>
                            <p>Your actual rate depends upon credit score, loan amount, loan term, and credit usage and history, and will be agreed upon between you and the lending partner. An example of total amount paid on a personal loan of $10,000 for a term of 36 months at a rate of 10% would be equivalent to $11,616.12 over the 36-month life of the loan.</p>
                            <p>Checking your rate generates a soft inquiry on your credit report, which is visible only to you. Our lending partners may perform a hard inquiry that could affect your credit score when you submit an application for credit.</p>
                            <p>The information you provide will be shared with lending partners and third parties in order to process your request.</p>
                            <p><img src="<?php site_url(); ?>/wp-content/uploads/2019/05/equal-housing-grey-1.png" alt="Equal Housing Opportunity" title="Equal Housing Opportunity"></p>
                            
                        </div>
                        
                    </div>
                    
                </div>


			<?php
			} //end nosocket check


		
		
		} //end blank & nofooterarea check
		?>
		<!-- end main -->
		</div>
		
		<?php
		
		if(avia_get_option('disable_post_nav') != "disable_post_nav")
		{
			//display link to previous and next portfolio entry
			echo avia_post_nav();
		}
		
		echo "<!-- end wrap_all --></div>";


		if(isset($avia_config['fullscreen_image']))
		{ ?>
			<!--[if lte IE 8]>
			<style type="text/css">
			.bg_container {
			-ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $avia_config['fullscreen_image']; ?>', sizingMethod='scale')";
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $avia_config['fullscreen_image']; ?>', sizingMethod='scale');
			}
			</style>
			<![endif]-->
		<?php
			echo "<div class='bg_container' style='background-image:url(".$avia_config['fullscreen_image'].");'></div>";
		}
	?>

    <?php if($_GET['se']): ?>
        
        <?php $section = check($_GET['se']); ?>
    
        <script>

            jQuery(function(){
                setTimeout(function(){ jQuery("html, body").animate({ scrollTop: jQuery('#<?php echo $section; ?>').offset().top - 88 }, 1000); }, 1000);
            });
        
        </script>
    
    <?php endif; ?>

<?php




	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */


	wp_footer();


?>
<a href='#top' title='<?php _e('Scroll to top','avia_framework'); ?>' id='scroll-top-link' <?php echo av_icon_string( 'scrolltop' ); ?>><span class="avia_hidden_link_text"><?php _e('Scroll to top','avia_framework'); ?></span></a>

<div id="fb-root"></div>

<?php if(get_field('script_footer', 'option')){the_field('script_footer', 'option');} ?>

<?php if(get_field('fo_script')){the_field('fo_script');} ?>

<?php if(isset($_POST["get_started_email"])): ?>

    <script>
        jQuery(function(){
            setTimeout(function(){
                jQuery('#input_1_2').val("<?php echo $_POST["get_started_email"]; ?>");
                jQuery("html, body").animate({ scrollTop: jQuery('#av_section_1').offset().top - 88 }, 1000); 
            }, 1000);
        });
    </script>

<?php endif; ?>

</body>
</html>
