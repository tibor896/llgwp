<?php 
global $avia_config;

$responsive		= avia_get_option('responsive_active') != "disabled" ? "responsive" : "fixed_layout";
$headerS 		= avia_header_setting();
$social_args 	= array('outside'=>'ul', 'inside'=>'li', 'append' => '');
$icons 			= !empty($headerS['header_social']) ? avia_social_media_icons($social_args, false) : "";

if(isset($headerS['disabled'])) return;

?>

<div class="header">

    <?php if(isset($_GET["is_user_new"])): ?>
        <a href="tel:+18005191130" class="header-phone">
            <h3><i class="fa fa-phone"></i> Call 24/7 <b>(800) 519-1130</b></h3>
        </a>
    <?php else: ?>
        <a href="tel:+18008035760" class="header-phone">
            <h3><i class="fa fa-phone"></i> Call 24/7 <b>(800) 803-5760</b></h3>
        </a>
    <?php endif; ?>
	
    <?php echo avia_logo(AVIA_BASE_URL.'images/layout/logo.png', '', 'div', true); ?>

</div>
